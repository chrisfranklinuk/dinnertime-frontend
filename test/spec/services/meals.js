'use strict';

describe('Service: meals', function () {

  // load the service's module
  beforeEach(module('dinnertimeApp'));

  // instantiate service
  var meals;
  beforeEach(inject(function (_meals_) {
    meals = _meals_;
  }));

  it('should do something', function () {
    expect(!!meals).toBe(true);
  });

});
