'use strict';

angular.module('dinnertimeApp')
  .factory('MealFactory', function ($resource) {
    return $resource('http://localhost:8000/api/meals/:id', {});
  });
