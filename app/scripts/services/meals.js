'use strict';

angular.module('dinnertimeApp')
  .factory('MealsFactory', function MealsFactory($resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function
  return $resource('http://localhost:8000/api/meals/', {});
  });
