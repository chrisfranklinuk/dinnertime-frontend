'use strict';

angular.module('dinnertimeApp')
  .controller('MealsCtrl', function ($scope, MealsFactory, $resource, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    //$scope.meals = $resource('http://localhost:8000/api/meals/');
    //$scope.meal = $resource('http://localhost:8000/api/meals/:mealId', {mealId:'@id'});

    // Instantiate an object to store your scope data in (Best Practices)
    $scope.data = {};

    MealsFactory.query(function(response) {
      // Assign the response INSIDE the callback
      $scope.data.meals = response;
    });

    // callback for ng-click 'editUser':
    $scope.mealDetail = function (Id) {
        $location.path('/meal/' + Id);
    };

    $scope.doSearch = function () {

        // $scope.mealResult = $scope.meal.get({mealId:1});
        // $scope.mealsResult = $scope.meals.get({});
        
      };
  });
