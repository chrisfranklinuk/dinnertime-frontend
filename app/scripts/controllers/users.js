'use strict';

angular.module('dinnertimeApp')
  .controller('UsersCtrl', function ($scope, UsersFactory, $resource) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    // Instantiate an object to store your scope data in (Best Practices)
    $scope.data = {};

    UsersFactory.query(function(response) {
      // Assign the response INSIDE the callback
      $scope.data.users = response;
    });

    $scope.doSearch = function () {

        
        // $scope.mealResult = $scope.meal.get({mealId:1});
        // $scope.usersResult = $scope.users.get({});
        
      };
  });
