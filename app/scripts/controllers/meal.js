'use strict';

angular.module('dinnertimeApp')
  .controller('MealCtrl', function ($scope, MealFactory, $resource, $location, $routeParams) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.data = {};
    
    

    MealFactory.get({id: $routeParams.id}, function(response) {
      // Assign the response INSIDE the callback
      $scope.data.meal = response;
    });
  });
